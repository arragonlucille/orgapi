<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertInitModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('init_modules')->insert(
            array(
                'title' => 'agenda',
                'component' => 'components/Agenda',
                'description' => 'Rappels & Evenements'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('init_modules')->orderBy('id', 'desc')->take(1)->delete();
    }
}
