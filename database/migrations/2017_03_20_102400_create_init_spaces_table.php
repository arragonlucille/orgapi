<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInitSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('init_spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
        });

        DB::table('init_spaces')->insert(
            array(
                'title' => 'maison',
                'description' => 'Espace regroupant toutes les données concernant la gestion de la maison et de la famille.'
            )
        );
        DB::table('init_spaces')->insert(
            array(
                'title' => 'travail',
                'description' => 'Espace dédié à la gestion de la partie travail. Très pratique pour les freelances.'
            )
        );
        DB::table('init_spaces')->insert(
            array(
                'title' => 'personnel',
                'description' => 'Espace personnel permettant de se consacrer à soi même.'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('init_spaces');
    }
}
