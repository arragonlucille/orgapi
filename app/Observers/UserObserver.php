<?php

namespace App\Observers;

use App\Models\User;
use App\Models\InitSpace;
use App\Models\Space;
use Illuminate\Support\Facades\DB;

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    // public function creating(User $user)
    // {
    //     $initSpaces = InitSpace::all()->makeHidden('id')->toArray(); //spaces initial
    //     $userSpaces = [];
    //     foreach ($initSpaces as $s) {
    //         $userSpaces[] = new Space($s);
    //     }
    //     var_dump($user);exit;
    //     DB::transaction(function () use ($user, $userSpaces) {
    //         $user->save();
    //         $user->spaces()->saveMany($userSpaces);
    //     });
    // }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    // public function deleting(User $user)
    // {
    //     //
    // }
}