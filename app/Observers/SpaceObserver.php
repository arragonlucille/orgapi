<?php

namespace App\Observers;

use App\Models\User;
use App\Models\InitSpace;
use App\Models\Space;
use Illuminate\Support\Facades\DB;

class SpaceObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function creating(Space $space)
    {
        // var_dump($space->slug);exit;
        // $initSpaces = InitSpace::all()->makeHidden('id')->toArray(); //spaces initial
        // $userSpaces = [];
        // foreach ($initSpaces as $s) {
        //     $userSpaces[] = new Space($s);
        // }
        // DB::transaction(function () use ($user, $userSpaces) {
        //     $user->spaces()->saveMany($userSpaces);
        // });
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    // public function deleting(User $user)
    // {
    //     //
    // }
}