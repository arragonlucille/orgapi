<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        $this->app['auth']->viaRequest('api', function ($request) {
        // var_dump($request->input(), User::where('api_token', $request->input('api_token'))->toSql());exit;
            // via api
            if ($request->input('api_token')) {
                return User::where('api_token', $request->input('api_token'))->first();
            }
            // via email / password
            if($request->input('email') && $request->input('password')){
                $user = User::where('email', $request->input('email'))->first();
                if($user){
                    $check = Hash::check($request->input('password'), $user->password);
                    if($check){
                        return $user;
                    }
                }
            }
        });
    }
}
