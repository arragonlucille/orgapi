<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Item extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /* Remplissable, editable*/
    protected $fillable = ['title', 'description', 'space_id', 'module_id', 'user_id'];

    /** Champs affiché - Json / Array **/
    protected $hidden = ['created_at', 'updated_at', 'itemable_id', 'itemable_type'];
    // protected $visible = ['title', 'description', 'space_id', 'items'];

    /** Champs ajoutés **/
    // protected $appends = ['title', 'description'];
    // protected $with = ['itemable'];

    public function module()
    {
        return $this->belongsTo('App\Models\Item');
    }

    public function itemable(){
        return $this->morphTo('itemable');
    }
}
