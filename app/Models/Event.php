<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Event extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'email', 'password'
    // ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $hidden = [
        // 'password',
    // ];
    /* Remplissable, editable*/
    protected $fillable = ['start_date', 'end_date', 'start_time', 'end_time', 'all_day'];

    /** Champs affiché - Json / Array **/
    protected $hidden = ['created_at', 'updated_at'];
    // protected $visible = ['title', 'description', 'space_id', 'items'];

    /** Champs ajoutés **/
    // protected $appends = ['title', 'description'];
    protected $with = ['item'];

    public function item(){
        // var_dump($this->morphOne('App\Models\Item', 'itemable', 'itemable_type', 'itemable_id', 'id'));
        return $this->morphOne('App\Models\Item', 'itemable', 'itemable_type', 'itemable_id', 'id');
    }

}
