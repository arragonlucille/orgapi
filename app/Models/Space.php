<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Space extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'slug'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    // protected $visible = ['id', 'title', 'description', 'slug'];

    protected $hidden = [
        'user_id', 'created_at', 'updated_at'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function modules(){
        // return $this->belongsToMany('App\Models\InitModule', 'modules', 'space_id', 'imodule_id')->withPivot('name');
        return $this->hasMany('App\Models\Module');
    }

    public function getTitleAttribute(){
        return ucwords($this->attributes['title']);
    }

    public function setSlugAttribute($value){
        $this->attributes['slug'] = str_slug($value, '-');
    }
}
