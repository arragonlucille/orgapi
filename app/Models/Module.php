<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Module extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /* Remplissable, editable*/
    protected $fillable = ['name', 'space_id', 'imodule_id'];

    /** Champs affiché - Json / Array **/
    protected $hidden = ['created_at', 'updated_at'];
    protected $visible = ['id', 'title', 'description', 'space_id', 'items', 'slug', 'component'];

    /** Champs ajoutés **/
    protected $appends = ['title', 'description', 'component'];
    // protected $attributes = ['title' => '', 'description' => ''];

    public function initModule(){
        return $this->belongsTo('App\Models\InitModule', 'imodule_id', 'id');
    }

    public function space(){
        return $this->belongsTo('App\Models\Space', 'space_id', 'id');
    }

    public function items(){
        return $this->hasMany('App\Models\Item', 'module_id', 'id');
    }

    /***
    * Les données sont complétés à partir des données du module initial
    ***/

    /**
     * Récupère le titre du module (personnalisé ou pas)
     */
    public function getTitleAttribute(){
        $returnName = '';
        $imodule = $this->initModule()->first();
        $returnName = $imodule->title;
        if(!empty($this->attributes['name'])){
            $returnName = $this->attributes['name'];
        }
        return ucwords($returnName);
    }

    /**
     * Récupère la description du module
     */
    public function getDescriptionAttribute(){
        $imodule = $this->initModule()->first();
        if(!empty($imodule)){
            return $imodule->description;
        }
        return '';
    }

    public function getComponentAttribute() {
        $imodule = $this->initModule()->first();
        return $imodule->component;
    }

    /**
     * Set le slug du module
     */
    public function setSlugAttribute($value){
        $this->attributes['slug'] = str_slug($value, '-');
    }
}
