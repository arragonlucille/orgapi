<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        // 'password',
    ];

    public function spaces(){
        return $this->hasMany('App\Models\Space');
    }

    /**
     * Enregistre le nouvelles utilisateurs et ses espaces
     */
    public function saveUserAndSpacesSet() {
        $initSpaces = InitSpace::all()->makeHidden('id')->toArray(); //spaces initial
        $userSpaces = [];
        foreach ($initSpaces as $s) {
            $tmpSpace = new Space($s);
            $tmpSpace->slug = $tmpSpace->title;
            $userSpaces[] = $tmpSpace;
        }
        // $this->spaces();
        // var_dump($this);exit;
    //     var_dump($user);exit;
        $that = $this;
        return DB::transaction(function () use ($that, $userSpaces) {
        //     var_dump($that);exit;
            $that->save();
            $that->spaces()->saveMany($userSpaces);
            return true;
        });
    }

}
