<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Space;
use Validator;

class SpacesController extends Controller
{
    /**
     * Listes les espaces de l'utilisateur.
     *
     * @return void
     */
    public function index()
    {
        $spaces = Space::where('user_id', Auth::user()->id)->with('modules', 'modules.initModule')->get();
        $messages = '';
        if(empty($spaces)){
            $messages = 'Aucun espace de données.';
        }
        return response()->json([
            'error' => false,
            'messages' => $messages,
            'datas' => $spaces
        ]);
    }

    public function store(Request $request)
    {
        $dataSpace = $request->only(['title', 'description']);
        $validator = Validator::make($dataSpace, [
            'title' => 'required',
            // 'description' => 'required',
        ]);
        if($validator->fails()){
            return response()->json([
                'error' => true,
                'messages' => $validator->errors()->messages()
            ], 400);
        }

        //Sauvegarde & réponse
        $newSpace = new Space($dataSpace);
        $newSpace->user_id = Auth::user()->id;
        if($newSpace->save()){
            return response()->json([
                'error' => false,
                'messages' => 'Space '.$newSpace->title.' save'
            ], 201);
        }

        //Erreur générale
        return response()->json([
            'error' => true,
            'messages' => 'Error store space.'
        ], 400);
    }

    public function show(Request $request, $spacename)
    {
        //Get Space datas
        // var_dump($spacename, Auth::user()->id);exit;
        $s = Space::with('modules', 'modules.initModule')->where([['user_id', Auth::user()->id], ['slug', $spacename]])->first();
        // var_dump($s->toArray());exit;
        if(!empty($s)){
            return response()->json([
                'error' => false,
                'datas' => $s
            ], 201);
        }

        //No datas
        return response()->json([
            'error' => true,
            'messages' => 'Space not found.'
        ], 400);
    }

    public function update(Request $request, $id)
    {
        //Get Data
        $dataSpace = $request->only(['title', 'description']);

        //Validation
        $validator = Validator::make($dataSpace, [
            'title' => 'required',
            // 'description' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'error' => true,
                'messages' => $validator->errors()->messages()
            ], 400);
        }

        //Set Data
        $space = Space::where('id', $id)->first();
        foreach ($dataSpace as $key => $value) {
            $space->$key = $value;
        }

        //Sauvegarde & réponse
        if($space->save()){
            return response()->json([
                'error' => false,
                'messages' => 'Space '.$space->title.' update.'
            ], 201);
        }

        //Erreur générale
        return response()->json([
            'error' => true,
            'messages' => 'Error update space.'
        ], 400);
    }
    
    public function destroy(Request $request, $id)
    {
        $space = Space::find($id);
        if(empty($space)){
            return response()->json([
                'error' => true,
                'messages' => 'Not found space to delete.'
            ], 400);
        }

        if($space->delete()){
            return response()->json([
                'error' => false,
                'messages' => 'Space '.$space->title.' delete.'
            ], 201);
        }
        //Error in deleting space
        return response()->json([
            'error' => true,
            'messages' => 'Error delete space.'
        ], 400);
    }

}
