<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\InitSpace;
use App\Models\Space;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    /**
     * Store the user in database.
     *
     * @param  int  $id
     * @return Response
     */
    public function store(Request $request)
    {
        //récupération des données
        $datas = $request->input();

        //Préparation de l'utilisateur à enregistrer.
        $new = new User($datas);
        $new->password = Hash::make($datas['password']);
        $new->api_token = str_random(60);

        //Test de la validité des données
        $validator = Validator::make($datas, [
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);

        //Réponse en cas d'erreur
        if($validator->fails()){
            return response()->json([
                'error' => true,
                'messages' => $validator->errors()->messages()
            ], 400);
        }

        // DB::transaction(function () use ($new) {
        //     $new->saveUserAndSpacesSet();
        // });

        //Sauvegarde & réponse
        if($new->saveUserAndSpacesSet()){
            return response()->json([
                'error' => false,
                'messages' => 'User '.$new->email.' save'
            ], 201);
        }

        //Erreur générale
        return response()->json([
            'error' => true,
            'messages' => 'Error store user.'
        ], 400);
    }

    public function login(Request $request)
    {
        if(Auth::check()){
            return response()->json([
                'error' => false,
                'datas' => Auth::user()->api_token,
                'messages' => 'User connected.'
            ], 200);
        }
        return response()->json([
            'error' => true,
            'messages' => 'Error login user.'
        ], 400);

    }
}