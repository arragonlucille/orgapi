<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Item;
class EventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index(Request $request)
    {
        $datas = $request->except('api_token');
        $forDaysDate = date_create('now');;
        $forDaysType = 'month';
        if(isset($datas['days_type']) && in_array($datas['days_type'], ['month', 'week', 'day'])){
            $forDaysType = $datas['days_type'];
        }
        if(isset($datas['days_date'])){
            $forDaysDate = date_create($datas['days_date']);
        }
        $messages = '';
        //Retourne tous les évenements d'un utilisateur, d'un espace ou d'un module.
        $events = Event::query();
        $events = $events->whereHas('item', function($query) use ($datas, $forDaysType, $forDaysDate){
            if(isset($datas['space_id'])){
                $query->where('space_id', $datas['space_id']);
            }
            if(isset($datas['module_id'])){
                $query->where('module_id', $datas['module_id']);
            }
            $query->where('user_id', Auth::user()->id);
        });
        // Retourne les evenements en fonction des dates
        switch ($forDaysType) {
            case 'day':
                $events->whereDate('start_date', $forDaysDate->format('Y-m-d'));
                $events->orWhere(function($query) use ($forDaysDate){
                    $query->whereDate('end_date', $forDaysDate->format('Y-m-d'));
                });
                break;
            case 'week':
                $weekStart = date_isodate_set($forDaysDate, $forDaysDate->format('Y'), $forDaysDate->format('W'));
                $tmpStart = $weekStart->format('Y-m-d');
                $weekEnd = date_modify($weekStart, '+6 days');
                $events->whereBetween('start_date', [$tmpStart, $weekEnd->format('Y-m-d')]);
                $events->orWhereBetween('end_date', [$tmpStart, $weekEnd->format('Y-m-d')]);
                break;
            
            case 'month':
            default:
                $startMonth = $forDaysDate->format('Y-m-01');
                $endMonth = $forDaysDate->format('Y-m-t');
                $events->whereBetween('start_date', [$startMonth, $endMonth]);
                $events->orWhereBetween('end_date', [$startMonth, $endMonth]);
                break;
        }
        if(empty($events)){
            $messages = 'No events.';
        }
        return response()->json([
            'error' => false,
            'messages' => $messages,
            'datas' => $events->get()
        ]);
    }

    public function store(Request $request){
        $newItem = new Item();
        $itemDatas = $request->only($newItem->getFillable());
        foreach ($itemDatas as $key => $value) {
            $newItem->$key = $value;
        }
        $newItem->user_id = Auth::user()->id;

        $newEvent = new Event();
        $eventDatas = $request->only($newEvent->getFillable());
        foreach ($eventDatas as $key => $value) {
            $newEvent->$key = $value;
        }

        $response = [
        'error' => true,
        'message' => 'Tout va mal...'
        ];
        $response = DB::transaction(function () use ($newEvent, $newItem) {
            $newEvent->save();
            $newItem->itemable()->associate($newEvent);
            $newItem->save();
            return ['error' => false, 'messages' => 'Tout va bien !'];
        });

        return response()->json($response);
    }

    public function show(Request $request, $id){
        var_dump('show Events');
        $obj = Event::where('id', $id)->with('item')->get();
        // var_dump($obj);
        return response()->json([
            'datas' => $obj
            ]);
    }

    //
}
