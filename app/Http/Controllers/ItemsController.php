<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

class ItemsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        $items = Item::all();
        var_dump($items);
    }

    public function store(Request $request){
        $datas = $request->except('api_token');
        var_dump($datas);
    }

    public function show(Request $request, $id){
        $i = Item::find($id);
        var_dump($i);
    }

    //
}
