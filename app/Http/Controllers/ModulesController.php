<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Module;
use App\Models\InitModule;

class ModulesController extends Controller
{

    public function index(){
        $modules = Module::with('space')->whereHas('space', function($query){
            $query->where('user_id', Auth::user()->id);
        })->get();
        return response()->json([
            'error' => false,
            // 'messages' => $messages,
            'datas' => $modules
        ]);
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function store(Request $request)
    {
        $datas = $request->only(['space_id', 'imodule_id', 'name']);

        $exist = Module::where('space_id', $datas['space_id'])->where('imodule_id', $datas['imodule_id'])->first();

        if(empty($exist)){
            $new = new Module;
            $new->space_id = $datas['space_id'];
            $new->imodule_id = $datas['imodule_id'];
            // var_dump($new->initModule->title);exit;
            $new->slug = $new->initModule->title;
            if(isset($datas['name'])) {
                $new->name = $datas['name'];
                $new->slug = $datas['name'];
            }
            $truc = DB::transaction(function () use ($new) {
                $new->save();
            });
            return response()->json([
                'error' => false,
                'messages' => 'Module add to space.'
            ]);
        }

        return response()->json([
            'error' => true,
            'messages' => 'Module exist in this space.'
        ]);
    }

    public function show(Request $request, $space_id, $modulename){

        $module = Module::where([['space_id', $space_id], ['slug', $modulename]])->with('items')->get();
        var_dump($module->toArray(), $space_id);exit;
        return response()->json([
            'error' => false,
            'datas' => $module
        ]);
    }

}
