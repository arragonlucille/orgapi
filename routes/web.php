<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
$app->post('user/login', ['uses' => 'AuthController@login']);
$app->post('user', ['uses' => 'AuthController@store']);

$app->group(['middleware' => 'auth'], function () use ($app) {
	$app->get('spaces', ['uses' => 'SpacesController@index']);
	$app->post('spaces', ['uses' => 'SpacesController@store']);
	$app->get('spaces/{space}', ['uses' => 'SpacesController@show']);
	$app->put('spaces/{space}', ['uses' => 'SpacesController@update']);
	$app->patch('spaces/{space}', ['uses' => 'SpacesController@update']);
	$app->delete('spaces/{space}', ['uses' => 'SpacesController@destroy']);

	$app->get('modules', ['uses' => 'ModulesController@index']);
	$app->post('modules', ['uses' => 'ModulesController@store']);
	$app->get('{space}/modules/{module}', ['uses' => 'ModulesController@show']);
	// $app->put('modules/{module}', ['uses' => 'ModulesController@update']);
	// $app->patch('modules/{module}', ['uses' => 'ModulesController@update']);
	// $app->delete('modules/{module}', ['uses' => 'ModulesController@destroy']);

	$app->get('events', ['uses' => 'EventsController@index']);
	$app->post('events', ['uses' => 'EventsController@store']);
	$app->get('events/{event}', ['uses' => 'EventsController@show']);
	$app->put('events/{event}', ['uses' => 'EventsController@update']);
	$app->patch('events/{event}', ['uses' => 'EventsController@update']);
	$app->delete('events/{event}', ['uses' => 'EventsController@destroy']);

	$app->get('items', ['uses' => 'ItemsController@index']);
	$app->post('items', ['uses' => 'ItemsController@store']);
	$app->get('items/{item}', ['uses' => 'ItemsController@show']);
	$app->put('items/{item}', ['uses' => 'ItemsController@update']);
	$app->patch('items/{item}', ['uses' => 'ItemsController@update']);
	$app->delete('items/{item}', ['uses' => 'ItemsController@destroy']);
// 	Route::resource('photo', 'PhotoController', ['only' => [
//     'index', 'show'
// ]]);
});
// $app->get('auth', function () use ($app) {});
